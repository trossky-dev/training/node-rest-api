var connection= require('./connection');

function MethodDB(){

    this.getArticleAll=function(response){
      connection.getMyConnection(function(err, cn){
          cn.query('select * from articles', function(error, result){
                cn.release();
                if(error){
                  response.send({state:'Error'});
                }else{
                  response.send(result);
                }

          });

      })
    }

    this.getArticleById=function(id,response){
      connection.getMyConnection(function(err, cn){
          cn.query('select * from articles where id=?',id, function(error, result){
                cn.release();
                if(error){
                  response.send({state:'Error'});
                }else{
                  response.send(result);
                }

          });

      })
    }


    this.createArticle=function(data,response){
      connection.getMyConnection(function(err, cn){
          cn.query('insert into articles set ?',data, function(error, result){
                cn.release();
                if(error){
                  response.send({state:'It cannot create!'});
                }else{
                  response.send({state:'ok'});
                }

          });

      })
    }


    this.deleteArticle=function(id,response){
      connection.getMyConnection(function(err, cn){
          cn.query('delete from articles where id= ?',id, function(error, result){
                cn.release();
                if(error){
                  response.send({state:'It cannot delete!'});
                }else{
                  response.send({state:'ok'});
                }

          });

      })
    }


    this.updateArticle=function(id,data,response){
      connection.getMyConnection(function(err, cn){
          cn.query('update articles set ? where id= ?',[data,id], function(error, result){
                cn.release();
                if(error){
                  response.send({state:'It cannot update!'});
                }else{
                  response.send({state:'ok'});
                }

          });

      })
    }





}
module.exports= new  MethodDB();
