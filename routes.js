var db= require('./queries');
const version_api='/v1/';

function http(){
    this.configuration=function(app){
        app.get((version_api+'articles/'),function(request,response){
          db.getArticleAll(response);
        });

        app.get(version_api+'articles/:id/',function(request,response){
          db.getArticleById(request.params.id, response);
        });

        app.post(version_api+'articles/',function(request,response){
          db.createArticle(request.body,response);
        });

        app.delete(version_api+'articles/:id/',function(request,response){
          db.deleteArticle(request.params.id,response);
        });

        app.put(version_api+'articles/:id/',function(request,response){
          db.updateArticle(request.params.id,request.body,response);
        });

    }
}

module.exports= new http();
