var express =require('express');
var bodyparser=require('body-parser');

var app= express();

app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());

var connection=require('./connection');
var routes=require('./routes');

connection.begin();
routes.configuration(app);


var server=app.listen(8000, function(){
  console.log('Escuchann por el puerto',server.address().port)
})
